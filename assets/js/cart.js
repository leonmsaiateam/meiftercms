 
$('.addToCart').click(function(){    
	
   $.ajax({ 

   		type: 'POST',
   		url:  '/addToCart', 
   		data: {
   			'prod_id': $(this).attr('data-prod') 	
   		},
   		success: function(result){
        	console.log(result);
    	}   
	});
});

$('.quantityUp').click(function(){    
  
  var prodQuantityElement = $(this).siblings('.quantityProd');
  var priceUnit = $(this).siblings('.priceProdUnit');
  var totalAmount = $(this).parent().siblings('.totalPerProd').children('.totalPriceProd');
    
  $.ajax({
   		type: 'POST',
   		url:  '/addOneProd', 
   		data: {
   			'prod_id': $(this).attr('data-prod') 	
   		},
   		success: function(result){  
        var txtVal  = +prodQuantityElement.val();          
      	prodQuantityElement.val(txtVal + 1);
        totalAmount.text(+prodQuantityElement.val() * priceUnit.val());
        calculateCartTotal();
    	}   
	});
  
});



$('.quantityDown').click(function(){    
		
  var prodQuantityElement = $(this).siblings('.quantityProd');
  var priceUnit = $(this).siblings('.priceProdUnit');
  var totalAmount =  $(this).parent().siblings('.totalPerProd').children('.totalPriceProd');

  //Si el valor era '1' y resta, se elimina el producto
  if(prodQuantityElement.val() == '1') {
       removeItem($(this));  
  } else {

      $.ajax({ 
          type: 'POST',
          url:  '/minusOneProd', 
          data: {
            'prod_id': $(this).attr('data-prod') ,  
            'quantity': prodQuantityElement.val() 
          },
          success: function(result){            
               
              var txtVal  = +prodQuantityElement.val();
              prodQuantityElement.val(txtVal - 1);
              totalAmount.text(+prodQuantityElement.val() * priceUnit.val());     
              calculateCartTotal();
          }   
      });
  }
 
});

$('.deleteCartProd').click(function(){
    removeItem($(this));          
});

function calculateCartTotal() {
    var total = 0;
    $('.totalPriceProd').each(function() {       
        total += +$(this).text();
    })
    $('#cartTotal').text(total);
}

function removeItem(elmt) {
    var r = confirm("Desea eliminar este producto de su carrito?");
    if (r) {
        $.ajax({
              type: 'POST',
              url:  '/deleteCartProd', 
              data: {
                'prod_id': elmt.attr('data-prod')          
              },
              success: function(result){            
                  if(result == 'BORRA') {
                      elmt.parent().parent().remove();  
                  }            
                  calculateCartTotal();
              }   
        });
    } 
}

$('.addToList').click(function(){   
   var prod_id = $(this).attr('data-prod');
   $.ajax({ 
      type: 'POST',
      url:  '/getMyLists', 
      success: function(result){
          var htmlString = '';                               
          $.each($.parseJSON(result), function(index, val){           
            htmlString += '<option value="' + val['id'] +'">' + val['descripcion'] +  '</tr>';
          });
          //dropdown lists 
          $('#myCreatedLists').html(htmlString);
          $('#prod_id_add').val(prod_id);
      }   
  });
});

$('.addProdToList').click(function(){ 
   
  $.ajax({ 
      type: 'POST',
      url:  '/addProdList', 
      data: {
          'list_id': $('#myCreatedLists').val(),
          'prod_id': $('#prod_id_add').val()          
      },
      success: function(result){
          $('#listMessage').html(result);
      }   
  });
});


$('.deleteList').click(function(){
    var r = confirm("Desea eliminar esta lista?");    
    if (r) {
        deleteList($(this));
    }         
});


$('.addListToCart').click(function(){
    var r = confirm("Desea agregar la lista a sus compras?");    
    if (r) {
        addListToCart($(this).attr('data-list'));
    }         
});

function deleteList(elmt) {
   
  $.ajax({
        type: 'POST',
        url:  '/deleteList', 
        data: {
          'list_id': $(elmt).attr('data-list')         
        },
        success: function(result){            
            if(result == 'BORRA') {
                elmt.parent().parent().remove();  
            }
        }   
  });

}

function addListToCart(idList) {
  
    $.ajax({
        type: 'POST',
        url:  '/addListToCart', 
        data: {
          'list_id': idList         
        },
        success: function(result){            
            console.log("lista añadida al cart");
        }   
    });
}

$('.listDetails').click(function(){
        $('.cart-list').html('');  
        var elmt = $(this) ;
      
        $.ajax({
              type: 'POST',
              url:  '/getListData', 
              data: {
                'list_id': elmt.attr('data-list')         
              },
              success: function(result){            
                var htmlString = '';  
                var feedback = '';               
                $.each($.parseJSON(result), function(index, val){
                    feedback = val['feedback'];
                    htmlString += '<tr>' +
                                    '<td>' +
                                      '<div class="img-container">' +
                                        '<img src="assets/uploads/files/products/' + val['pic'] + '" alt="' + val['name'] + '" style="float: left; width: 100%;"/>' +
                                      '</div>' +
                                    '</td>' +
                                    '<td class="td-name">' + val['name'] + '</td>' +
                                    '<td>' + val['desc'] + '</td>' +
                                    '<td class="td-number"><small>$</small>' + val['price'] + '</td>' +
                                    '<td class="td-number"><small>x</small>' + val['quantity'] + '</td>' +
                                    '<td class="td-number"><small>$</small><span class="totalPriceProd">' + val['price'] *  val['quantity'] + '</span></td>' +
                                  '</tr>';
                });
 
                htmlString += '<tr>' +
                                '<td colspan="6"> Total: <span id="cartTotal" /></span> </td>' +
                              '</tr>' ;
                              if(feedback != '' && feedback != null) {
                              htmlString += '<tr>' +
                                '<td colspan="6">"' + feedback + '"</td>' +
                              '</tr>' ;
                              }
                              htmlString += '<tr>' +
                                 '<td>' +
                                    '<button onclick="addListToCart(' + elmt.attr('data-list') + ');" type="button" rel="tooltip" title="Agregar a Lista de Compras" class="btn btn-success btn-simple btn-xs">' +
                                    '<i class="fa fa-heart"></i>' +
                                    '</button>' +
                                  '</td>' +
                              '</tr>'; 

                $('.cart-list').append(htmlString);                 
                calculateCartTotal();     
            }   
        });
            
});

$('.feedbackLink').click(function() {

  var list_id = $(this).attr('data-list') ;
  $('#listIdHidden').val(list_id);
  
}); 

$('#promoCodeInsert').change(function(){    
  var discCode = $(this).val()

  $.ajax({
      type: 'POST',
      url:  '/checkPromoCodeNfo', 
      data: {
        'disc_code': discCode,
      },
      success: function(result){  
        if (result != '') {
          var resultado = $.parseJSON(result);
          var discountValue = resultado['disc_percent'];
          $('.promoCodeValid').show();
          $('.promoCodeNone').hide();
          $('.discountShow').show();
          var initVal = $('#cartTotal').text();
          var finalVal = initVal - ((discountValue * $('#cartTotal').text())/100);
          $('.regularPricePromoCode').append(initVal);
          $('.discPricePromoCode').append(finalVal);
        }else{
          $('.promoCodeNone').show();
          $('.promoCodeValid').hide();
        };
      }   
  });
});
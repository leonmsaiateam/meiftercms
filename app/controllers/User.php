<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	public function __construct(){
		parent::__construct();	
		$this->load->model('Commercial_model');
		$this->load->model('Internal_model');
		$this->load->model('User_model');
		$this->load->model('Location_Model');
	}

	public function regUser() {
		$regname = $_POST['name'];
		$reglastname = $_POST['lastname'];
		$regphone = $_POST['phone'];
		$regmail = $_POST['mail'];
		$regpass = $_POST['pass'];
		$paisID = $_POST['paisID'];
		$provinciaID = $_POST['provinciaID'];
		$partidoID = $_POST['partidoID'];
		$localidadID = $_POST['localidadID'];

		if (isset($_POST['barrioID'])) {
			$barrioID = $_POST['barrioID'];
		}else{
			$barrioID = null;
		}

		if (isset($_POST['subbarrioID'])) {
			$subbarrioID = $_POST['subbarrioID'];
		}else{
			$subbarrioID = null;
		}

		$additional_data = array(
			'first_name' => $regname,
			'last_name' => $reglastname,
			'username' => $regmail,
			'pais' => $paisID,
			'provincia' => $provinciaID,
			'partido' => $partidoID,
			'localidad' => $localidadID,
			'barrio' => $barrioID,
			'subbarrio' => $subbarrioID
		);
		$group = array('2');

		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End
		
		// Get Basic Info For Page Meta
		$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Bienvenido';
		$data['charset'] = getSiteConfiguration()['site_charset'];
		$data['description'] = getSiteConfiguration()['site_desc'];
		$data['keywords'] = getSiteConfiguration()['site_keywords'];
		$data['language'] = getSiteConfiguration()['site_lang'];
		$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
		$data['favicon'] = getSiteConfiguration()['site_favicon'];
		$data['author'] = getSiteConfiguration()['site_author'];

		// Location Script
		$data['pais'] = $this->Location_Model->getPais();
		// Location Script End

		$this->ion_auth->register($regmail, $regpass, $regmail, $additional_data, $group);

		$this->load->view('auth/welcome', $data);
	}

	public function logUser() {

		$identity = $_POST['logmail'];
		$password = $_POST['logpass'];

		if(isset($_POST['logrem'])){
		  $remember = 1;
		}else{
		  $remember = 0;
		}

		$this->ion_auth->login($identity, $password, $remember);
		//echo $identity . ' ' . $password . ' ' . $remember;
		
		if ($this->ion_auth->login($identity, $password, $remember))
		{	
			redirect('Home','refresh');
		}
		else
		{
			// Navbar Configuration
			$data['navbarConf'] = 'commercial';
			// Navbar Configuration End
			// Load Renders for Navbar
			$data['menuCat'] = $this->Commercial_model->getCategoryALL();
			$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
			$data['menuInt'] = $this->Internal_model->getInternalALL();
			// Load Renders for Navbar End
			
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Iniciar Sesion';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			$data['loginFail'] = TRUE;

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Load View
			$this->load->view('user/login', $data);
		}

	}

	public function logUserOut() {
		$this->ion_auth->logout();
		redirect('Home','refresh');
	}

	public function mustLog()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {
			
			redirect('Home','refresh');

		}else{
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Necesitas Logearte';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Load View
			$this->load->view('auth/mustlogin', $data);
		}
	}

	public function login()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {
			
			redirect('Home','refresh');

		}else{
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Iniciar Sesion';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			$data['loginFail'] = FALSE;

			// Load View
			$this->load->view('user/login', $data);
		}
	}

	public function register()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {
			
			redirect('Home','refresh');

		}else{
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Registrarse';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Load View
			$this->load->view('user/register', $data);
		}
	}

	public function personalInfo()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {
			// Handle User Info
			$userID = $this->ion_auth->get_user_id();
			$data['userNfo'] = $this->User_model->getByID($userID);
			
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Informacion Personal';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Load View
			$this->load->view('User/personaldata', $data);
		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}
	}

	public function changePersonalNfo()
	{
		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {

		$userID = $this->ion_auth->get_user_id();
		$username =  $_POST['usernameMod'];
		$firstname =  $_POST['fnameMod'];
		$lastname =  $_POST['lnameMod'];
		$about =  $_POST['aboutMod'];
		$company =  $_POST['companyMod'];
		$adress =  $_POST['adressMod'];

		$pais =  $_POST['pais'];
		$provincia =  $_POST['provincia'];
		$partido =  $_POST['partido'];
		$localidad =  $_POST['localidad'];
		$barrio =  $_POST['barrio'];
		$subbarrio =  $_POST['subbarrio'];

		$zipcode =  $_POST['zipMod'];
		$phone =  $_POST['phoneMod'];
		$email =  $_POST['emailMod'];

		$data = array(
           'username' => $username,
           'first_name' => $firstname,
           'last_name' => $lastname,
           'about' => $about,
           'company' => $company,
           'adress' => $adress,
           'pais' => $pais,
           'provincia' => $provincia,
           'partido' => $partido,
           'localidad' => $localidad,
           'barrio' => $barrio,
           'subbarrio' => $subbarrio,
           'zip' => $zipcode,
           'phone' => $phone,
           'email' => $email
        );

		$this->db->where('id', $userID);
		$this->db->update('users', $data); 
		redirect('User/personalInfo','refresh');

		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}

	}

	public function securityInfo()
	{
		// Navbar Configuration
		$data['navbarConf'] = 'commercial';
		// Navbar Configuration End
		// Load Renders for Navbar
		$data['menuCat'] = $this->Commercial_model->getCategoryALL();
		$data['menuBrands'] = $this->Commercial_model->getBrandsALL();
		$data['menuInt'] = $this->Internal_model->getInternalALL();
		// Load Renders for Navbar End

		// Render Title and Tags
		if ($this->ion_auth->logged_in()) {
			// Handle User Info
			$userID = $this->ion_auth->get_user_id();
			$data['userNfo'] = $this->User_model->getByID($userID);
			
			$data['userID'] = $userID;
			// Get Basic Info For Page Meta
			$data['title'] = getSiteConfiguration()['site_name'] . ' | ' . 'Informacion de Seguridad';
			$data['charset'] = getSiteConfiguration()['site_charset'];
			$data['description'] = getSiteConfiguration()['site_desc'];
			$data['keywords'] = getSiteConfiguration()['site_keywords'];
			$data['language'] = getSiteConfiguration()['site_lang'];
			$data['appleicon'] = getSiteConfiguration()['site_appleicon'];
			$data['favicon'] = getSiteConfiguration()['site_favicon'];
			$data['author'] = getSiteConfiguration()['site_author'];

			// Location Script
			$data['pais'] = $this->Location_Model->getPais();
			// Location Script End

			// Load View
			$this->load->view('User/securityInfo', $data);
		}else{
			// Error Forbiden Access
			redirect('User/mustLog','refresh');
		}
	}

}
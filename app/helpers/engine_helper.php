<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// Core Engine for Meifter CMS
// Created by CodeMeifter
// All Rights Reserved.
// 2016

define('LIST_TYPE_BUY', 'cart'); 
define('LIST_TYPE_USER_LIST', 'list'); 
define('SALE_TYPE_CONTRAENTREGA', 'Contraentrega'); 
define('SALE_TYPE_PAYU', 'PayU'); 
define('STATUS_SALE_DELIVERY_PENDING', 'Pendiente'); 
define('STATUS_SALE_DELIVERY_OK', 'Entregado'); 
define('STATUS_SALE_DELIVERY_CANCELED', 'Cancelado'); 

// Operative Functions
function getMyID()
{
    $ci =& get_instance();
    $user = $ci->ion_auth->user()->row();
    if(isset($user->id)) { 
        return $user->id;
    }else{	
    	return null;
    }
}
function getUserNfo($user_id)
{
    $ci =& get_instance();
    $ci->db->from('users');
    $ci->db->where('id', $user_id);
    $user = $ci->db->get();
    $userNfo = null;
    if ($user->num_rows() > 0)
    {
        foreach ($user->result() as $p)
        {
            $userNfo = array(
                'id' => $p->id,
                'ip_address' => $p->ip_address,
                'username' => $p->username,
                'password' => $p->password,
                'salt' => $p->salt,
                'email' => $p->email,
                'activation_code' => $p->activation_code,
                'forgotten_password_code' => $p->forgotten_password_code,
                'forgotten_password_time' => $p->forgotten_password_time,
                'remember_code' => $p->remember_code,
                'created_on' => $p->created_on,
                'last_login' => $p->last_login,
                'active' => $p->active,
                'first_name' => $p->first_name,
                'last_name' => $p->last_name,
                'image' => $p->image_path,
                'company' => $p->company,
                'phone' => $p->phone,
                'adress' => $p->adress,
                'pais' => $p->pais,
                'provincia' => $p->provincia,
                'partido' => $p->partido,
                'localidad' => $p->localidad,
                'barrio' => $p->barrio,
                'subbarrio' => $p->subbarrio,
                'zip' => $p->zip,
                'about' => $p->about,
                'userPic' => $p->image_path
            );
        }
    }
    return $userNfo;
}

// Get Brand Info
function getBrandNfo($brand_ID)
{
    $ci =& get_instance();
    $ci->db->from('prod_brand');
    $ci->db->where('prod_brand_id', $brand_ID);
    $user = $ci->db->get();
    $brandNfo = null;
    if ($user->num_rows() > 0)
    {
        foreach ($user->result() as $p)
        {
            $brandNfo = array(
                'title' => $p->title,
                'slug' => $p->slug,
                'desc' => $p->desc,
            );
        }
    }
    return $brandNfo;
}

// Get Product Info
function getProductNfo($product_id)
{
    $ci =& get_instance();
    $ci->db->from('product');
    $ci->db->where('prod_id', $product_id);
    $prod = $ci->db->get();
    $prodNfo = null;
    if ($prod->num_rows() > 0)
    {
        foreach ($prod->result() as $prod)
        {
            $prodNfo = array(
                'name' => $prod->name,
                'slug' => $prod->slug,
                'desc' => $prod->desc,
                'short_desc' => $prod->short_desc,
                'pic' => $prod->pic,
                'price' => $prod->price,
                'tax_id' => $prod->tax_id,
                'brand_id' => $prod->brand_id,
                'cat_id' => $prod->cat_id
            );
        }
    }
    return $prodNfo;
}

function getProductSegment($product_id)
{
    $ci =& get_instance();
    $ci->db->from('prod_seg');
    $ci->db->where('prod_id', $product_id);
    $segmentProd = $ci->db->get();
    $prodSegmentList = array();
    if ($segmentProd->num_rows() > 0)
    {
        foreach ($segmentProd->result() as $sgpr)
        {
            $segmentName = getSegmentByID($sgpr->prod_segment_id)['symbol'];
            array_push($prodSegmentList, $segmentName);
        }
    }

    return $prodSegmentList;
}

function getSegmentByID($prod_segment_id)
{
    $ci =& get_instance();
    $ci->db->from('prod_segment');
    $ci->db->where('prod_segment_id', $prod_segment_id);
    $segmentNfo = $ci->db->get();
    $segmentNfoList = null;
    if ($segmentNfo->num_rows() > 0)
    {
        foreach ($segmentNfo->result() as $sgpr)
        {
            $segmentNfoList = array(
                'title' => $sgpr->title,
                'symbol' => $sgpr->symbol,
                'slug' => $sgpr->slug,
                'desc' => $sgpr->desc
            );
        }
    }

    return $segmentNfoList;
}

// Segment Functions

// Cart Process Info
function calcAllCart($id)
{
    $ci =& get_instance();
    $ci->db->from('cart');
    $ci->db->where('usr_id', $id);
    $itemsCant = $ci->db->get();
    
    $totalCantCart = 0;


    // Get Total Number of Items in Cart
    if ($itemsCant->num_rows() > 0)
    {
        foreach ($itemsCant->result() as $p)
        {
            $totalCantCart = $totalCantCart + $p->prod_quantity;
        }
    }

    // Get Total Amount of Cash in Cart
    $totalCashCart = 0;
    if ($itemsCant->num_rows() > 0)
    {
        foreach ($itemsCant->result() as $p)
        {
            $totalCashCart = $totalCashCart + (getProductNfo($p->prod_id)['price'] * $p->prod_quantity);
        }
    }

    // Cart Information
    $cartNfo = array(
        'cartItems' => $totalCantCart,
        'cartCash' => $totalCashCart,
    );

    return $cartNfo;
}

// Blog Process
function getPostNfo($post_id, $user_id)
{
    $ci =& get_instance();
    $ci->db->from('post');
    $ci->db->where('post_id', $post_id);
    $blogNfo = $ci->db->get();
    $postResult = null;


    if ($blogNfo->num_rows() > 0)
    {
        foreach ($blogNfo->result() as $p)
        {
            $postResult = array(

            );
        }
    }
    return $postResult;
}
function getCatPostNfo($categorie_id)
{
    $ci =& get_instance();
    $ci->db->from('post_categorie');
    $ci->db->where('categorie_id', $categorie_id);
    $pcatnfo = $ci->db->get();
    $blogCatNfo = null;


    if ($pcatnfo->num_rows() > 0)
    {
        foreach ($pcatnfo->result() as $p)
        {
            $blogCatNfo = array(
                'title' => $p->title,
                'slug' => $p->slug,
                'desc' => $p->desc,
                'image' => $p->image
            );
        }
    }
    return $blogCatNfo;
}

function getCurrencyByID($currencyID)
{
    $ci =& get_instance();
    $ci->db->from('currency_list');
    $ci->db->where('currency_id', $currencyID);
    $currency = $ci->db->get();
    $currencyVal = null;
    if ($currency->num_rows() > 0)
    {
        foreach ($currency->result() as $cur)
        {
            $currencyVal = array(
                'name' => $cur->name,
                'iso' => $cur->iso,
                'symbol' => $cur->symbol
            );
        }
    }
    return $currencyVal;
}

function getTaxByID($taxID)
{
    $ci =& get_instance();
    $ci->db->from('tax_prod');
    $ci->db->where('tax_id', $taxID);
    $tax = $ci->db->get();
    $taxValue = null;
    if ($tax->num_rows() > 0)
    {
        foreach ($tax->result() as $tx)
        {
            $taxValue = array(
                'title' => $tx->title,
                'value' => $tx->value
            );
        }
    }
    return $taxValue;
}

function getStoreConfiguration()
{
    $ci =& get_instance();
    $ci->db->from('store_conf');
    $storeConf = $ci->db->get();
    $storeConfVal = null;
    if ($storeConf->num_rows() > 0)
    {
        foreach ($storeConf->result() as $stconf)
        {
            $storeConfVal = array(
                'currency' => getCurrencyByID($stconf->currency_id)['symbol'],
                'tax' => getTaxByID($stconf->tax_id)['value'],
                'payustate' => $stconf->disable_payu,
                'stock' => $stconf->show_stock,
                'shipping' => $stconf->show_shipping
            );
        }
    }
    return $storeConfVal;
}

function getSiteConfiguration()
{
    $ci =& get_instance();
    $ci->db->from('site_conf');
    $siteConf = $ci->db->get();
    $siteConfVal = null;
    if ($siteConf->num_rows() > 0)
    {
        foreach ($siteConf->result() as $stconf)
        {
            $siteConfVal = array(
                'site_name' => $stconf->site_name,
                'site_author' => $stconf->site_author,
                'site_desc' => $stconf->site_desc,
                'site_favicon' => $stconf->site_favicon,
                'site_logo' => $stconf->site_logo,
                'site_logofoot' => $stconf->site_logofoot,
                'site_charset' => $stconf->site_charset,
                'site_lang' => $stconf->site_lang,
                'cooming_soon' => $stconf->cooming_soon,
                'site_keywords' => $stconf->site_keywords,
                'site_appleicon' => $stconf->site_appleicon,
                'site_mail' => $stconf->site_mail
            );
        }
    }
    return $siteConfVal;
}

function getPayMethods()
{
    $ci =& get_instance();
    $ci->db->from('pay_mods');
    $icon = $ci->db->get();
    if ($icon->num_rows() > 0)
    {
        foreach ($icon->result() as $pymtd)
        {
            echo '<div class="col-md-4 paymods">';
            echo    '<img src="' . base_url() . 'assets/uploads/files/icons/' . $pymtd->icon . '" alt="">';
            echo '</div>';
            
        }
    }
}

function getRelProdByPost($post_id)
{
    $ci =& get_instance();
    $ci->db->from('prod_post');
    $ci->db->where('post_id', $post_id);
    $prodPost = $ci->db->get();
    if ($prodPost->num_rows() > 0)
    {
        foreach ($prodPost->result() as $pID)
        {
            $ci =& get_instance();
            $ci->db->from('product');
            $ci->db->where('prod_id', $pID->prod_id);
            $prodPost = $ci->db->get();
            if ($prodPost->num_rows() > 0)
            {
                foreach ($prodPost->result() as $prod)
                {
                    echo '<div class="col-md-3 highlight-dest relpostprod">';
                    echo '<a href="' . base_url() . 'product/' . $prod->slug . '">';
                    echo   '<img src="' . base_url() . 'assets/uploads/files/products/' . $prod->pic . '" alt="' . $prod->name . '"/>';
                    echo   '<h3 class="title">' . $prod->name . '</h3>';
                    echo   '<p class="description">' . $prod->short_desc . '</p>';
                    echo   '<span class="price">' . getStoreConfiguration()['currency'] . ' ' . $prod->price . '</span>';
                    echo '</a>';
                    echo '</div>';
            
                }
            }
        }
    }
}

// Get Products Nfo
function getMinProductPrice()
{
    $ci =& get_instance();
    $ci->db->select_min('price');
    $ci->db->limit(1);
    $minval = $ci->db->get('product');
    if ($minval->num_rows() > 0)
    {
        foreach ($minval->result() as $min)
        {
            $minPrice = $min->price;
        }
    }
    
    return $minPrice;
}
function getMaxProductPrice()
{
    $ci =& get_instance();
    $ci->db->select_max('price');
    $ci->db->limit(1);
    $minval = $ci->db->get('product');
    if ($minval->num_rows() > 0)
    {
        foreach ($minval->result() as $min)
        {
            $minPrice = $min->price;
        }
    }
    
    return $minPrice;
}

function checkRelProdByPost($post_id)
{
    $ci =& get_instance();
    $ci->db->from('prod_post');
    $ci->db->where('post_id', $post_id);
    $prodPost = $ci->db->get();
    if ($prodPost->num_rows() > 0)
    {
        return true;
    }else{
        return false;
    }
}


// Get Category
function getCatBlogList()
{
    $ci =& get_instance();
    $ci->db->from('post_categorie');
    $blogCat = $ci->db->get();
    if ($blogCat->num_rows() > 0)
    {
        foreach ($blogCat->result() as $cat)
        {
            echo '<li>';
            echo   '<a href="' . base_url() . 'blog/category/' . $cat->slug . '">';
            echo     $cat->title;
            echo   '</a>';
            echo '</li>';
        }
    }
}

// Social Plugins Addons
function makeFbBtn($shareURL, $type){
    $callbackJS = "this.href, 'Facebook', 'width=640,height=580'";
    
    if ($type == 1) {
        $fbBtn = '<a href="https://www.facebook.com/sharer/sharer.php?u=' . $shareURL . '" class="btn btn-social btn-round btn-fill btn-facebook" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-facebook"></i>' .
                 '</a>';
    }
    if ($type == 2) {
        $fbBtn = '<a href="https://www.facebook.com/sharer/sharer.php?u=' . $shareURL . '" class="btn btn-social btn-round" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-facebook"></i>' .
                 '</a>';
    }
    if ($type == 3) {
        $fbBtn = '<a href="https://www.facebook.com/sharer/sharer.php?u=' . $shareURL . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
    }
    if ($type == 4) {
        $fbBtn = '<li><a href="https://www.facebook.com/sharer/sharer.php?u=' . $shareURL . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-facebook"></i> Compartir</a></li>';
    }
    echo $fbBtn;
}
function makeTwBtn($shareURL, $type){
    $callbackJS = "this.href, 'Facebook', 'width=640,height=580'";

    if ($type == 1) {
        $twBtn = '<a href="https://twitter.com/home?status=' . $shareURL . '" class="btn btn-social btn-round btn-fill btn-twitter" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-twitter"></i>' .
                 '</a>';
    }
    if ($type == 2) {
        $twBtn = '<a href="https://twitter.com/home?status=' . $shareURL . '" class="btn btn-social btn-round" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-twitter"></i>' .
                 '</a>';
    }
    if ($type == 3) {
        $twBtn = '<a href="https://twitter.com/home?status=' . $shareURL . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
    }
    if ($type == 4) {
        $twBtn = '<li><a href="https://twitter.com/home?status=' . $shareURL . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-twitter"></i> Tweetear</a></li>';
    }
    echo $twBtn;
}

function makePntBtn($imgPin, $shareURL, $type){
    $callbackJS = "this.href, 'Facebook', 'width=640,height=580'";
    $siteDescription =  getSiteConfiguration()['site_desc'];

    if ($imgPin == null) {
        $imgPin = base_url() . 'assets/img/' . getSiteConfiguration()['site_appleicon'];
    }

    if ($type == 1) {
        $pntBtn = '<a href="https://pinterest.com/pin/create/button/?url=' . $shareURL . '&media=' . $imgPin . '&description=' . $siteDescription . '" class="btn btn-social btn-round btn-fill btn-pinterest" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-pinterest-p"></i>' .
                 '</a>';
    }
    if ($type == 2) {
        $pntBtn = '<a href="https://pinterest.com/pin/create/button/?url=' . $shareURL . '&media=' . $imgPin . '&description=' . $siteDescription . '" class="btn btn-social btn-round" onclick="return !window.open(' . $callbackJS . ')">' .
                     '<i class="fa fa-pinterest-p"></i>' .
                 '</a>';
    }
    if ($type == 3) {
        $pntBtn = '<a href="https://pinterest.com/pin/create/button/?url=' . $shareURL . '&media=' . $imgPin . '&description=' . $siteDescription . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>';
    }
    if ($type == 4) {
        $pntBtn = '<li><a href="https://pinterest.com/pin/create/button/?url=' . $shareURL . '&media=' . $imgPin . '&description=' . $siteDescription . '" onclick="return !window.open(' . $callbackJS . ')"><i class="fa fa-pinterest"></i> Pinear</a></li>';
    }
    echo $pntBtn;
}

// Get Promo Code Nfo
function getPromoCodeNfoByCode($promoCode)
{
    $ci =& get_instance();
    $ci->db->from('promo_code');
    $ci->db->where('disc_code', $promoCode);
    $codeNfo = $ci->db->get();
    $promoCodeValues = null;
    if ($codeNfo->num_rows() > 0)
    {
        foreach ($codeNfo->result() as $pmc)
        {
            $promoCodeValues = array(
                'disc_name' => $pmc->disc_name,
                'disc_code' => $pmc->disc_code,
                'disc_percent' => $pmc->disc_percent,
                'disc_from' => $pmc->disc_from,
                'disc_to' => $pmc->disc_to
            );
        }
        return $promoCodeValues;
    }else{
        return null;
    }
}

// Get Mail Configuration
function getMailConfiguration()
{
    $ci =& get_instance();
    $ci->db->from('smtp_mail_conf');
    $ci->db->where('smtp_conf_id', 1);
    $malRend = $ci->db->get();
    $mlCnfVal = null;
    if ($malRend->num_rows() > 0)
    {
        foreach ($malRend->result() as $mlCnf)
        {
            $mlCnfVal = array(
                'smtp_host' => $mlCnf->smtp_host,
                'smtp_port' => $mlCnf->smtp_port,
                'smtp_timeout' => $mlCnf->smtp_timeout,
                'smtp_user' => $mlCnf->smtp_user,
                'smtp_pass' => $mlCnf->smtp_pass,
                'smtp_charset' => $mlCnf->smtp_charset,
                'smtp_newline' => $mlCnf->smtp_newline,
                'smtp_mailtype' => $mlCnf->smtp_mailtype,
                'smtp_validation' => $mlCnf->smtp_validation
            );
        }
        return $mlCnfVal;
    }else{
        return null;
    }
}


function getCatLinkBreadcrum($catID)
{
    $ci =& get_instance();
    $ci->db->from('prod_categorie');
    $ci->db->where('prod_cat_id', $catID);
    $catSlug = $ci->db->get();
    if ($catSlug->num_rows() > 0)
    {
        foreach ($catSlug->result() as $slg)
        {
            return '<a href="' . base_url() . 'category/' . $slg->slug . '">' . $slg->title . '</a>';
        }
        
    }
}
<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container contact-bg">
           <div class="row">
            <div class="col-md-6">
               <h2 class="section-title kaleTitle">Contacto</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi tempore vero quam, velit cumque fugit eius, recusandae facilis quo hic distinctio voluptatum. Tenetur nemo necessitatibus, illo itaque, deserunt vel quae.</p>
                <form id="contactForm" action="<?php echo base_url();?>Contact/contactSent" method="post" accept-charset="utf-8">
                      <div class="form-group">
                          <input name="email" class="form-control" id="contactEmail" type="email" placeholder="Email (requerido)" required>
                      </div>
                      <div class="form-group">
                          <input name="subject" class="form-control" id="contactSubject" type="text" placeholder="Asunto (requerido)">
                      </div>
                      <div class="form-group">
                          <textarea name="message" class="form-control" id="contactMessage" placeholder="Mensaje (requerido)" rows="4"></textarea>
                      </div>
                      <button type="submit" class="btn btn-info btn-fill btn-block" id="contactSend">Enviar</button>
                  </form>
                  <div id="loadingDiv" style="width:100%;float:left;display:none;">
                      <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Iniciando Sesion....
                  </div>
                  <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Inicio sesion exitosamente</span>
                  <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
  <script>
    // Attach a submit handler to the form
    $( "#contactForm" ).submit(function( event ) {       
      event.preventDefault();
      // Get some values from elements on the page:
      var $form = $( this );            
      var urlForm = $form.attr( "action" );
        var name = $("#contactName").val();
      var email = $("#contactEmail").val();
      var subject = $("#contactSubject").val();
      var message = $("#contactMessage").val();
      
      $.ajax({
        method: "POST",
        url: urlForm,
        data: { name: name, email: email, subject: subject, message: message },
        beforeSend: function() {
            $('#loadingDiv').show();
          },
      }).done(function() {
        $('#loadingDiv').hide();
        $('#contactOk').css('display', 'block');
        $('#contactSend').prop('disabled', true);
      })
      .fail(function() {
        $('#loadingDiv').hide();
        $('#contactError').css('display', 'block');
        $('#contactSend').prop('disabled', true);
      });         
    });
  </script>
</html>
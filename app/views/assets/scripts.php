<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>

<script>
// Provincia, Localidad, Partido Combo
$("#paisID").change(function() {
    paisID = $('#paisID').val();
    $("#paisID option:selected").each(function() {
        $.post("/location/getProvincia", {
            paisID : paisID
        }, function(data) {          
            $("#provinciaID").html(data);
        });
    });
});
$("#provinciaID").change(function() {
    provinciaID = $('#provinciaID').val();
    $("#provinciaID option:selected").each(function() {
        $.post("/location/getPartido", {
            provinciaID : provinciaID
        }, function(data) {          
            $("#partidoID").html(data);
        });
    });
});
$("#partidoID").change(function() {
    partidoID = $('#partidoID').val();
    $("#partidoID option:selected").each(function() {
        $.post("/location/getLocalidad", {
            partidoID : partidoID
        }, function(data) {          
            $("#localidadID").html(data);
        });
    });
});
$("#localidadID").change(function() {
    localidadID = $('#localidadID').val();
    $("#localidadID option:selected").each(function() {
        $.post("/location/getBarrio", {
            localidadID : localidadID
        }, function(data) {          
            $("#barrioID").html(data);
        });
    });
});
$("#barrioID").change(function() {
    barrioID = $('#barrioID').val();
    $("#barrioID option:selected").each(function() {
        $.post("/location/getSubbarrio", {
            barrioID : barrioID
        }, function(data) {          
            $("#subbarrioID").html(data);
        });
    });
});
// Provincia, Localidad, Partido Combo End
</script>

<?php if ($navbarConf == 'main'): ?>
<script>
	$().ready(function(){
	  $(window).on('scroll', gsdk.checkScrollForTransparentNavbar);
	});  
</script>
<?php else: ?>
<?php endif ?>
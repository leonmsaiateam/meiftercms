<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Iniciar Sesion</h2>
           <div class="row">
            <div class="col-md-12">

              <?php if ($loginFail == TRUE): ?>
                  <div class="row"> 
                    <span class="alert alert-danger alert-mail col-md-12" id="contactError">Usuario y/o Contraseña incorrecta</span>
                  </div>
              <?php endif ?>

              <form id="loginForm" action="<?php echo base_url();?>User/logUser" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" name="logmail" id="logmail" value="" placeholder="E-Mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="logpass" id="logpass" value="" placeholder="Contraseña" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="checkbox checkbox-blue" for="logrem">
                        <input type="checkbox" name="logrem" id="logrem" data-toggle="checkbox">
                        Mantener Sesion
                        </label>
                    </div>
                    <div class="form-group">
                        <a href="">¿Olvido su Contraseña?</a>
                    </div>
                    <button type="submit" class="btn btn-info btn-fill btn-block">Iniciar Sesion</button>
                </form>
                <div id="loadingDiv" style="width:100%;float:left;display:none;">
                    <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Iniciando Sesion....
                </div>
                <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Inicio sesion exitosamente</span>
                <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Seguridad</h2>
           <div class="row">
            <div class="col-md-12"> 
                <form id="loginForm" action="<?php echo base_url();?>Auth/change_password" method="post" accept-charset="utf-8">
                    <legend>Cambio de Contraseña</legend>
                    <div class="form-group">
                        <label for="old_password">Antigua Contraseña</label>
                        <input type="password" name="old_password" id="old" value="" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="new_password">Nueva Contraseña</label>
                        <input type="password" name="new_password" id="new" value="" placeholder="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="new_password_confirm">Confirmar Nueva Contraseña</label>
                        <input type="password" name="new_password_confirm" id="new_confirm" value="" placeholder="" class="form-control">
                    </div>
                    <input type="hidden" name="user_id" value="<?php echo $userID;?>" id="user_id">
                    <button type="submit" class="btn btn-info btn-fill btn-block">Guardar</button>
                </form>
              <div id="loadingDiv" style="width:100%;float:left;display:none;">
                  <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Guardando Datos....
              </div>
              <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se cambiaron los datos exitosamente</span>
              <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Datos Personales</h2>
           <div class="row">
            <div class="col-md-12">
              <?php foreach ($userNfo->result() as $usd): ?>
                <form id="loginForm" action="<?php echo base_url();?>User/changePersonalNfo" method="post" accept-charset="utf-8">
                    <legend>Informacion Personal</legend>
                    <div class="form-group">
                        <label for="usernameMod">Nombre de Usuario</label>
                        <input type="text" name="usernameMod" id="usernameMod" value="<?php echo $usd->username;?>" placeholder="Nombre de Usuario" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="fnameMod">Nombre</label>
                        <input type="text" name="fnameMod" id="fnameMod" value="<?php echo $usd->first_name;?>" placeholder="Nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="lnameMod">Apellido</label>
                        <input type="text" name="lnameMod" id="lnameMod" value="<?php echo $usd->last_name;?>" placeholder="Apellido" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="aboutMod">Sobre mi</label>
                        <input type="text" name="aboutMod" id="aboutMod" value="<?php echo $usd->about;?>" placeholder="Sobre mi" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="companyMod">Compañia</label>
                        <input type="text" name="companyMod" id="companyMod" value="<?php echo $usd->company;?>" placeholder="Compañia" class="form-control">
                    </div>
                    <legend>Informacion de Ubicacion</legend>
                    <div class="form-group">
                        <label for="adressMod">Direccion</label>
                        <input type="text" name="adressMod" id="adressMod" value="<?php echo $usd->adress;?>" placeholder="Direccion" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="paisID">Pais</label>
                        <select id="paisID" name="paisID" class="form-control">
                          <option value="">Seleccione un Pais</option>
                          <?php foreach ($pais->result() as $sg): ?>
                            <option value="<?php echo $sg->id; ?>"><?php echo $sg->nombre; ?></option>  
                          <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="provinciaID">Provincia</label>
                        <select id="provinciaID" name="provinciaID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="partidoID">Partido</label>
                        <select id="partidoID" name="partidoID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="localidadID">Localidad</label>
                        <select id="localidadID" name="localidadID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="barrioID">Barrio</label>
                        <select id="barrioID" name="barrioID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="subbarrioID">Sub Barrio</label>
                        <select id="subbarrioID" name="subbarrioID" class="form-control">
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="zipMod">Codigo Postal</label>
                        <input type="text" name="zipMod" id="zipMod" value="<?php echo $usd->zip;?>" placeholder="Codigo Postal" class="form-control">
                    </div>
                    <legend>Informacion de Contacto</legend>
                    <div class="form-group">
                        <label for="phoneMod">Telefono</label>
                        <input type="text" name="phoneMod" id="phoneMod" value="<?php echo $usd->phone;?>" placeholder="Telefono" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="emailMod">E-Mail</label>
                        <input type="text" name="emailMod" id="emailMod" value="<?php echo $usd->email;?>" placeholder="E-Mail" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-info btn-fill btn-block">Guardar</button>
                </form>
              <?php endforeach ?>
              <div id="loadingDiv" style="width:100%;float:left;display:none;">
                  <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Guardando Datos....
              </div>
              <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se cambiaron los datos exitosamente</span>
              <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
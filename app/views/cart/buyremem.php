<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
           <div class="container">
               <h2 class="section-title">Lista de Compras Frecuentes</h2>
               
               <div class="row">
                 <div class="col-md-9">
                   <p>Desde esta Seccion, usted podra crear sus listas de compras, para despues poder realizar las mismas de una forma rapida, facil y divertida.</p>
                 </div>
                 <div class="col-md-3">
                    <a href="#" class="btn btn-info btn-fill btn-block" data-toggle="modal" data-target="#createListModal" > Crear lista </a>
                 </div>
               </div>
               <div class="row">
                <?php if(isset($message) && !empty($message)) : ?>
                   <div class="col-md-12"><?php echo $message ?></div>
                <?php endif; ?>
                   <div class="col-md-12">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th class="text-center">
                                #
                              </th>
                              <th>
                                Fecha
                              </th>
                              <th>
                                Descripcion
                              </th>
                    <?php /*  <th class="text-right">
                                 Valor
                              </th>  */ ?>
                              <th class="text-right">
                                Acciones
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php 
                                $i = 1;
                                if ( is_array( $userLists) && !empty($userLists)) { 
                                  foreach ($userLists as $list) { ?>
                                    <tr>
                                      <td class="text-center">
                                        <?php echo $i ?>
                                      </td>
                                      <td>
                                        <?php  echo $list->date; ?>
                                      </td>
                                      <td>
                                        <?php  echo $list->descripcion; ?>
                                      </td>
                            <?php /*  <td class="text-right">
                                        <?php  echo $list->descripcion; ?>
                                      </td>  */ ?>
                                      <td class="td-actions text-right">
                                        <button data-list="<?php  echo $list->id; ?>" data-toggle="modal" data-target="#listModal" type="button" rel="tooltip" title="Ver Compra" class="listDetails btn btn-info btn-simple btn-xs">
                                        <i class="fa fa-edit"></i>
                                        </button>
                                        <button data-list="<?php  echo $list->id; ?>"  type="button" rel="tooltip" title="Agregar a Lista de Compras" class="btn btn-success btn-simple btn-xs">
                                        <i class="fa fa-heart"></i>
                                        </button>
                                        <button data-list="<?php  echo $list->id; ?>" type="button" rel="tooltip" title="Remover" class="deleteList btn btn-danger btn-simple btn-xs">
                                        <i class="fa fa-times"></i>
                                        </button>
                                      </td>
                                    </tr> 
                                <?php 
                                  }
                                } ?>


                      </tbody>
                    </table>   
                  </div>
                </div>
               </div>
           </div>
    </div><!-- section -->

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
    <?php $this->load->view('elements/list-view');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
  <script src="<?php echo base_url(); ?>assets/js/cart.js"></script>
</html>
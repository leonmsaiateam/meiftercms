<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
           <div class="container">
               <h2 class="section-title">Compras Realizadas</h2>
               <div class="row">
                <?php if(isset($message) && !empty($message)) : ?>
                   <div class="col-md-12"><?php echo $message ?></div>
                <?php endif; ?>
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table">
                    <thead>
                      <tr>
                        <th class="text-center">
                          #
                        </th>
                        <th>
                          Fecha
                        </th>
                        <th>
                          Descripcion
                        </th>
                        <th class="text-right">
                          Valor
                        </th>
                        <th class="text-right">
                          Acciones
                        </th>
                      </tr>
                    </thead>
                    <tbody>                      
                      <?php $i = 1 ?>
                      <?php if ( is_array( $buyLists) && !empty($buyLists)): ?>
                        <?php foreach ($buyLists as $list): ?>
                          <tr>
                            <td class="text-center">
                              <?php echo $i ?>
                            </td>
                            <td>
                              <?php  echo $list->date; ?>
                            </td>
                            <td>
                              <?php  echo $list->descripcion; ?>
                            </td>
                            <td class="text-right">
                              <?php  echo $list->total_amount; ?>
                            </td>
                            <td class="td-actions text-right">
                              <button data-toggle="modal" data-list="<?php  echo $list->id; ?>" data-target="#listFeedback" type="button" rel="tooltip" title="Deja tu comentario" class="feedbackLink btn btn-info btn-simple btn-xs">
                              <i class="fa fa-edit"></i>
                              </button> 
                              <button data-toggle="modal" data-list="<?php  echo $list->id; ?>" data-target="#listModal" type="button" rel="tooltip" title="Ver Compra" class="listDetails btn btn-info btn-simple btn-xs">
                              <i class="fa fa-edit"></i>
                              </button> 
                              <button data-list="<?php  echo $list->id; ?>" type="button" rel="tooltip" title="Agregar a Lista de Compras" class="addListToCart btn btn-success btn-simple btn-xs">
                              <i class="fa fa-heart"></i>
                              </button>
                              <button data-list="<?php  echo $list->id; ?>" type="button" rel="tooltip" title="Remover" class="deleteList btn btn-danger btn-simple btn-xs">
                              <i class="fa fa-times"></i>
                              </button>
                            </td>
                          </tr>
                        <?php $i = $i + 1;?>

                        <?php endforeach ?>
                      <?php endif ?>
                    </tbody>
                    </table>
                  </div>
                </div>
               </div>
           </div>
    </div>

    <div class="space-50"></div>     
    <?php $this->load->view('elements/footer');?>
    <?php $this->load->view('elements/list-view');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
  <script src="<?php echo base_url(); ?>assets/js/cart.js"></script>
</html>


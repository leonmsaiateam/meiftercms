<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section">
        <div class="container">
            <h2 class="section-title">Usted va Abonar con PayU!</h2>
            <div class="row">
                <div class="col-md-12">
                    <p>
                        Para abonar la compra, por favor, presione el boton de PayU y siga las instrucciones de su Sitio Web.
                    </p>
					<?php $amountValue = $total_amount; ?>
					<?php $amountCurrency = 'ARS'; ?>
                    <?php $refCode = getCode(); ?>
					<?php $signatureNotEncoded = getPayUStats()["APIkey"] . '~' . getPayUStats()["merchantID"] . '~' . $refCode . '~' . $amountValue . '~' . $amountCurrency;?>
					<?php $signatureEncoded = encodeMD5($signatureNotEncoded); ?>
                    <?php if (getPayUStats()['test'] == TRUE): ?>
                    <form method="post" action="https://sandbox.gateway.payulatam.com/ppp-web-gateway/">
                    <?php else: ?>
                    <form method="post" action="https://gateway.payulatam.com/ppp-web-gateway/">
                    <?php endif ?>
					  <input name="merchantId"    type="hidden"  value="<?php echo getPayUStats()['merchantID'];?>"   >
					  <input name="accountId"     type="hidden"  value="<?php echo getPayUStats()['accountID'];?>" >
					  <input name="description"   type="hidden"  value="<?php echo getPayUStats()["desc"]; ?>"  >
					  <input name="referenceCode" type="hidden"  value="<?php echo $refCode; ?>" >
					  <input name="amount"        type="hidden"  value="<?php echo $amountValue; ?>"   >
					  <input name="tax"           type="hidden"  value="0"  >
					  <input name="taxReturnBase" type="hidden"  value="0" >
					  <input name="currency"      type="hidden"  value="<?php echo $amountCurrency; ?>" >
					  <input name="signature"     type="hidden"  value="<?php echo $signatureEncoded; ?>"  >
					  <input name="test"          type="hidden"  value="<?php echo getPayUStats()['test']; ?>" >
					  <input name="buyerEmail"    type="hidden"  value="<?php echo getUserNfo(getMyID())['email'];?>" >
					  <input name="responseUrl"    type="hidden"  value="<?php echo base_url() . 'cart/payuresponse';?>" >
					  <input name="confirmationUrl"    type="hidden"  value="http://www.test.com/confirmation" >
					  <input name="Submit" class="btn btn-info btn-fill btn-block" type="submit"  value="Pagar con PayU" >
					</form>
                </div>
            </div>
        </div>
    </div>           
</div>
<div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
    <?php $this->load->view('assets/scripts');?>    
</html>
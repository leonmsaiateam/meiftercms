<?php
$ApiKey = getPayUStats()["APIkey"];
$merchant_id = $_REQUEST['merchantId'];
$referenceCode = $_REQUEST['referenceCode'];
$TX_VALUE = $_REQUEST['TX_VALUE'];
$New_value = number_format($TX_VALUE, 1, '.', '');
$currency = $_REQUEST['currency'];
$transactionState = $_REQUEST['transactionState'];
$firma_cadena = "$ApiKey~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
$firmacreada = md5($firma_cadena);
$firma = $_REQUEST['signature'];
$reference_pol = $_REQUEST['reference_pol'];
$cus = $_REQUEST['cus'];
$extra1 = $_REQUEST['description'];
$pseBank = $_REQUEST['pseBank'];
$lapPaymentMethod = $_REQUEST['lapPaymentMethod'];
$transactionId = $_REQUEST['transactionId'];
if ($_REQUEST['transactionState'] == 4 ) {
	$estadoTx = "Transacción aprobada";
}
else if ($_REQUEST['transactionState'] == 6 ) {
	$estadoTx = "Transacción rechazada";
}
else if ($_REQUEST['transactionState'] == 104 ) {
	$estadoTx = "Error";
}
else if ($_REQUEST['transactionState'] == 7 ) {
	$estadoTx = "Transacción pendiente";
}
else {
	$estadoTx=$_REQUEST['mensaje'];
}

$payuTransNfo = array(
	'user_id' =>getMyID(),
	'estadoTx' => $estadoTx,
	'transactionId' => $transactionId,
	'reference_pol' => $reference_pol,
	'referenceCode' => $referenceCode,
	'pseBank' => $pseBank,
	'cus' => $cus,
	'TX_VALUE' => $TX_VALUE,
	'currency' => $currency,
	'extra1' => $extra1,
	'lapPaymentMethod' => $lapPaymentMethod,
);
savePayuTrans($payuTransNfo);
?>
<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section">
        <div class="container">
            <h2 class="section-title">Resumen Transacción</h2>
            <div class="row">
                <div class="col-md-12">
                	<?php if (strtoupper($firma) == strtoupper($firmacreada)): ?>
					<div class="table-responsive">
					    <table class="table">
					        <tbody>
					            <tr>
					                <td>
					                    Estado de la transaccion
					                </td>
					                <td>
					                    <?php echo $estadoTx; ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    ID de la transaccion
					                </td>
					                <td>
					                    <?php echo $transactionId; ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Referencia de la venta
					                </td>
					                <td>
					                    <?php echo $reference_pol; ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Referencia de la transaccion
					                </td>
					                <td>
					                    <?php echo $referenceCode; ?>
					                </td>
					            </tr>
					                
					            <?php if ($pseBank != null): ?>
					            <tr>
					                <td>
					                    Cus
					                </td>
					                <td>
					                    <?php echo $cus; ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Banco
					                </td>
					                <td>
					                    <?php echo $pseBank; ?>
					                </td>
					            </tr>
					            <?php endif ?>

					            <tr>
					                <td>
					                    Valor total
					                </td>
					                <td>
					                    $<?php echo number_format($TX_VALUE); ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Moneda
					                </td>
					                <td>
					                    <?php echo $currency; ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Descripción
					                </td>
					                <td>
					                    <?php echo ($extra1); ?>
					                </td>
					            </tr>
					            <tr>
					                <td>
					                    Entidad
					                </td>
					                <td>
					                    <?php echo ($lapPaymentMethod); ?>
					                </td>
					            </tr>
					        </tbody>
					    </table>
					</div>
					<button class="btn btn-info btn-fill btn-block">Imprimir</button>
					<?php else: ?>
					<div class="row">
						<div class="col-md-12">
							<h1>Error validando firma digital.</h1>
						</div>
					</div>					
					<?php endif ?>
                </div>
            </div>
        </div>
    </div>           
</div>
<div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
    <?php $this->load->view('assets/scripts');?>    
</html>
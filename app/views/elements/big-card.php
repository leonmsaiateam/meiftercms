<div class="parallax filter-black">
    <div class="parallax-image">
        <img src="../assets/img/thumb.jpg" alt="..." />
    </div>
    <div class="small-info">
        <h1>Meifter CMS <small>v 2.0</small></h1>
        <h5>Desarrollado por CodeMeifter S.R.L. De desarrolladores para desarrolladores.</h5>
    </div>
</div>
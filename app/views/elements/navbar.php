<?php if ($navbarConf == 'main'): ?>
  <nav class="kalenav navbar navbar-transparent navbar-fixed-top" role="navigation">
<?php else: ?>
  <nav class="kalenav navbar navbar-default" role="navigation">
<?php endif ?>
    <div class="container">
    <div class="navbar-header">
      <button id="menu-toggle" type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar bar1"></span>
        <span class="icon-bar bar2"></span>
        <span class="icon-bar bar3"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url();?>">
        <img src="<?php echo base_url();?>assets/img/logo2.svg" alt="">
      </a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Tienda 
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu dropdown-with-icons">
                <?php foreach ($menuCat->result() as $mc): ?>
                  <li>
                      <a href="<?php echo base_url() . 'category/' . $mc->slug; ?>">
                           <?php echo $mc->title; ?>
                      </a>
                  </li>
                <?php endforeach ?>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Blog 
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu dropdown-with-icons">
                <?php getCatBlogList(); ?>
              </ul>
            </li>
            
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Secciones <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="/contact">
                      Contacto
                    </a>
                  </li>
                  <li>
                    <a href="/internal/faq">
                      Preguntas Frecuentes
                    </a>
                  </li>
                  <?php foreach ($menuInt->result() as $mi): ?>
                    <?php if ($mi->menu_disp == 1): ?>
                      <li>
                          <a href="<?php echo base_url() . 'page/' . $mi->slug; ?>">
                               <?php echo $mi->title; ?>
                          </a>
                      </li>
                    <?php endif ?>
                  <?php endforeach ?>
                </ul>
              </li>
            <?php if ($this->ion_auth->logged_in()): ?>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Mi Carrito [<?php echo calcAllCart(getMyID())['cartItems']; ?>] | Total $<?php echo calcAllCart(getMyID())['cartCash']; ?> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo base_url() . 'cart/getCart';?>">Ver Carrito</a></li>
                  <li><a href="<?php echo base_url() . 'cart/getBuys';?>">Compras Realizadas</a></li>
                  <li><a href="<?php echo base_url() . 'cart/getLists';?>">Lista de Compras</a></li>
                  <?php if (havePayu(getMyID()) == TRUE): ?>
                  <li><a href="<?php echo base_url() . 'cart/payuinvoice';?>">Recibos PayU</a></li>
                  <?php endif ?>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Usuario <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="<?php echo base_url() . 'User/personalInfo';?>">Datos Personales</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url() . 'User/securityInfo';?>">Configuracion de Seguridad</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url() . 'User/logUserOut';?>">
                      Salir
                    </a>
                  </li>
                </ul>
              </li>
              <?php else: ?>
              <li>
                <a id="registerScreen" href="#" data-toggle="modal" data-target="#registModal">
                  Registrarse
                </a>
              </li>
              <li>
                <a id="loginScreen" href="#" data-toggle="modal" data-target="#loginModal">
                  Iniciar Sesion
                </a>
              </li>
              <?php endif ?>
       </ul>
    </div>
  </div>
</nav>
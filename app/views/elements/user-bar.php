<?php if ($this->ion_auth->logged_in()): ?>
  <?php if ($navbarConf == 'main'): ?>
  <nav class="navbar indexNavbar userNavbar navbar-icons">
  <?php else: ?>    
  <nav class="navbar userNavbar navbar-icons">
  <?php endif ?>
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Mis Compras <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url() . 'cart/getBuys';?>">Compras Realizadas</a></li>
              <li><a href="<?php echo base_url() . 'cart/getLists';?>">Lista de Compras</a></li>
              <?php if (havePayu(getMyID()) == TRUE): ?>
              <li><a href="<?php echo base_url() . 'cart/payuinvoice';?>">Recibos PayU</a></li>
              <?php endif ?>
            </ul>
          </li>
          <li>
            <a href="<?php echo base_url() . 'cart/getCart';?>">
              Mi Carrito [<?php echo calcAllCart(getMyID())['cartItems']; ?>] | Total $<?php echo calcAllCart(getMyID())['cartCash']; ?>
            </a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              Configuracion <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url() . 'User/personalInfo';?>">Datos Personales</a></li>
              <li><a href="<?php echo base_url() . 'User/securityInfo';?>">Configuracion de Seguridad</a></li>
            </ul>
          </li>
          <li>
            <a href="<?php echo base_url() . 'User/logUserOut';?>">
              Salir
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<?php endif ?>
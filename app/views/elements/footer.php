<div class="space-50"></div>
<div class="section section-gray foot-bg" id="footers" style="padding-bottom: 0;">
    <footer class="footer footer-big">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo base_url();?>assets/img/logo1.svg" alt="">
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="title">kale</h5>
                            <nav>
                                <ul>
                                    <li>
                                        <a href="<?php echo base_url();?>" >
                                            Home
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" >
                                            Productos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>blog/category/consejos" >
                                            Consejos Nutricionales
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>page/quienes-somos" >
                                            Quienes Somos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>page/como-comprar" >
                                            Como Comprar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>page/promos" >
                                            Promos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>internal/faq" >
                                            Preguntas Frecuentes
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>blog/category/recetario" >
                                            Recetario
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-6">
                            <h5 class="title">contacto</h5>
                            <nav>
                                <ul>
                                    <li>
                                        011 1234-5678
                                    </li>
                                    <li>
                                        info@kale.com.ar
                                    </li>
                                    <li>
                                        Calle, Direccion
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <h5 class="title">medios de pago</h5>
                    <div class="row">
                        <?php getPayMethods(); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <h5 class="title">certificacion</h5>
                    <img class="afipIMG" src="<?php echo base_url();?>assets/img/afip.jpg" alt="">
                </div>
            </div>

            <hr />
            <div class="social-area text-center">
                    <h5>Comparti con nosotros</h5>
                    <?php $urlTarget = current_url();?>
                    <?php makeFbBtn($urlTarget, 2); ?>
                    <?php makeTwBtn($urlTarget, 2); ?>
                    <?php makePntBtn(null, $urlTarget, 2) ?>
                </div>

            <div class="copyright">
                &copy; 2016, <?php echo getSiteConfiguration()['site_name'];?>. Todos los derechos reservados.
            </div>
        </div>
    </footer>
</div>

<div class="modal modal-small fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Iniciar Sesion</h4>
            </div>
            <div class="modal-body">
                <form id="loginForm" action="<?php echo base_url();?>User/logUser" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" name="logmail" id="logmail" value="" placeholder="E-Mail" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" name="logpass" id="logpass" value="" placeholder="Contraseña" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="checkbox checkbox-blue" for="logrem">
                        <input type="checkbox" name="logrem" id="logrem" data-toggle="checkbox">
                        Mantener Sesion
                        </label>
                    </div>
                    <div class="form-group">
                        <a href="">¿Olvido su Contraseña?</a>
                    </div>
                    <button type="submit" class="btn btn-info btn-fill btn-block">Iniciar Sesion</button>
                </form>
                <div id="loadingDiv" style="width:100%;float:left;display:none;">
                    <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Iniciando Sesion....
                </div>
                <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Inicio sesion exitosamente</span>
                <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-small fade" id="registModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Registrarse</h4>
            </div>
            <div class="modal-body">
                <form id="registForm" action="<?php echo base_url();?>User/regUser" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <input type="text" value="" id="regname" name="name" placeholder="Nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="reglastname" name="lastname" placeholder="Apellido" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="regphone" name="phone" placeholder="Telefono" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="text" value="" id="regmail" name="mail" placeholder="E-Mail" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <select id="paisID" name="paisID" class="form-control">
                          <option value="">Seleccione un Pais</option>
                          <?php foreach ($pais->result() as $sg): ?>
                            <option value="<?php echo $sg->id; ?>"><?php echo $sg->nombre; ?></option>  
                          <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="provinciaID" name="provinciaID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="partidoID" name="partidoID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="localidadID" name="localidadID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="barrioID" name="barrioID" class="form-control">
                        </select>
                    </div>
                    <div class="form-group">
                        <select id="subbarrioID" name="subbarrioID" class="form-control">
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="password" value="" id="regpass" name="pass" placeholder="Contraseña" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" value="" id="regrepeatpass" name="rpass" placeholder="Repetir Contraseña" class="form-control">
                    </div>
                    <span class="alert alert-danger alert-mail" role="alert" id="incorrectPass" style="display:none;">Las Contraseñas no son iguales</span>
                    <button type="submit" class="btn btn-info btn-fill btn-block">Crear Cuenta</button>
                </form>
                <div id="loadingDiv" style="width:100%;float:left;display:none;">
                    <img src="<?php echo base_url();?>assets/img/AjaxLoader.gif"> Registrando....
                </div>
                <span class="alert alert-success alert-mail" role="alert" id="contactOk" style="display:none;">Se registro con Exito</span>
                <span class="alert alert-danger alert-mail" role="alert" id="contactError" style="display:none;">Se produjo un error</span>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-small fade" id="addToList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Añadi el producto a tus listas</h4>
            </div>
            <div class="modal-body">
                <div>                    
                    Crea una nueva lista
                    <form action="<?php echo base_url();?>Cart/newList" method="post" accept-charset="utf-8">
                        <div class="form-group">
                            <input type="text" name="listName" value="" placeholder="" class="form-control">
                        </div>                   
                        <button type="submit" class="btn btn-info btn-fill btn-block">Crear</button>
                    </form>
                </div>   
                <input type="hidden" id="prod_id_add"></div>
                <div class="selectList">
                    <select id="myCreatedLists"></select>                
                    <button type="button" class="btn btn-info btn-fill btn-block addProdToList">Añadir</button>
                </div>
                <div id="listMessage"></div>
            </div>
        </div>
    </div>
</div>

 

<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Esta seccion es solo para usuarios registrados</h2>
           <div class="row">
            <div class="col-md-12">
              <p>Por favor, <a href="<?php echo base_url() . 'User/login';?>">inicie sesion</a> o <a href="<?php echo base_url() . 'User/register';?>">registrese</a>.</p>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
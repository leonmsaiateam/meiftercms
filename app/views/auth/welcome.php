<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    
    <div class="section">
       <div class="container">
           <h2 class="section-title">Iniciar Sesion</h2>
           <div class="row">
            <div class="col-md-12">
            	<h1 class="title">Bienvenido a <?php echo getSiteConfiguration()['site_name'];?></h1>
				<p>Gracias por registrarse, se envio a su casilla un mail para confirmar su cuenta.</p>
            </div>
           </div>
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section section-description">
      <div class="container">
          
          <h2 class="section-title"><?php echo lang('forgot_password_heading');?></h2>
          <div class="row">
            <div class="col-md-12">
              <h5><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></h5>
              <div id="infoMessage"><?php echo $message;?></div>
              <div class="textcontainer">
				<form id="loginForm" action="<?php echo base_url();?>auth/forgot_password" method="post" accept-charset="utf-8">
                    <div class="form-group">
                        <?php echo form_input($identity);?>
                    </div>
                    <?php echo form_submit('submit', lang('forgot_password_submit_btn'));?>
                </form>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
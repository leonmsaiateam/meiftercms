<!doctype html>
<html lang="en">
<head>
  <meta charset="<?php echo $charset;?>" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $appleicon;?>">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $favicon;?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title><?php echo $title;?></title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="<?php echo $description;?>">
  <meta name="keywords" content="<?php echo $keywords;?>">
  <meta name="author" content="<?php echo $author;?>">
  <link href="<?php echo base_url(); ?>assets/css/undercore.css" rel="stylesheet" />
  <link href="<?php echo base_url(); ?>assets/css/under.css" rel="stylesheet" />
  <link href='https://fonts.googleapis.com/css?family=Grand+Hotel|Open+Sans:400,300' rel='stylesheet' type='text/css'>
  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-transparent navbar-fixed-top" role="navigation">  
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
          <?php
            $urlTarget = current_url();
          ?>
          <?php makeFbBtn($urlTarget, 4); ?>
          <?php makeTwBtn($urlTarget, 4); ?>
          <?php makePntBtn(null, $urlTarget, 4) ?>
       </ul>
    </div>
  </div>
</nav>
<div class="main" style="background-image: url('<?php echo base_url(); ?>assets/img/underbg.jpg')">   
    <div class="cover black" data-color="black"></div>
    <div class="container">
        <div class="logo cursive logoIMG">
            <img src="<?php echo base_url();?>assets/img/logo1.svg" alt="">
        </div>
        <div class="content">
            <h4 class="motto">En este momento estamos realizando tareas de mantenimiento.</h4>
            <div class="subscribe">
                <h5 class="info-text">
                    Por favor, volve mas tarde para enterarte de todas nuestras novedades.
                </h5>
            </div>
        </div>
    </div>
    <div class="footer">
      <div class="container">
        © 2016, Kale. Todos los derechos reservados.
      </div>
    </div>
 </div>
 </body>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
</html>
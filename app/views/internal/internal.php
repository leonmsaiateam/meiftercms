<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section section-description">
      <div class="container">

        <?php foreach ($internal->result() as $int): ?>
          
          <h2 class="section-title"><?php echo $int->title; ?></h2>
          <div class="row">
            <div class="col-md-12">
              <h5><?php echo $int->subtitle; ?></h5>
              <div class="textcontainer">
                <?php echo $int->text; ?>
              </div>
            </div>
          </div>
        <?php endforeach ?>

      </div>
    </div>
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
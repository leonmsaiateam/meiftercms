<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="section">
       <div class="container faq-bg">
           <div class="row">
            <div class="col-md-6">
               <h2 class="section-title kaleTitle">Preguntas Frecuentes</h2>
               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi tempore vero quam, velit cumque fugit eius, recusandae facilis quo hic distinctio voluptatum. Tenetur nemo necessitatibus, illo itaque, deserunt vel quae.</p>
                

                <div id="acordeon">
                  <div class="panel-group" id="accordion">
                    <?php $numerator = 0; ?>
                    <?php foreach ($faq->result() as $sg): ?>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                        <a data-target="#collapse-<?php echo $numerator;?>" href="#collapse-<?php echo $numerator;?>" data-toggle="gsdk-collapse">
                          <?php echo $sg->question; ?>
                        </a>
                        </h4>
                      </div>
                      <div id="collapse-<?php echo $numerator;?>" class="panel-collapse collapse">
                        <div class="panel-body">
                           <?php echo $sg->answer; ?>
                        </div>
                      </div>
                    </div>
                    <?php $numerator = $numerator ++; ?>
                    <?php endforeach ?>
                  </div>
                </div>


            </div>
           </div>
       </div>
    </div>
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
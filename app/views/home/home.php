<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">
    <div class="parallax">
        <div class="parallax-image">
            <img src="../assets/img/home.jpg" alt="..." />
        </div>
    </div>
    <div class="section section-description">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="section-title">Sobre Kale</h2>
            <div class="row">
              <div class="col-md-12">
                <h5>Easy to integrate</h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio repellendus, odit incidunt officiis soluta cupiditate quisquam dicta accusamus aspernatur quod tenetur. Dolorum voluptatibus dolor eius, accusantium maiores ea vitae et.
                </p>
                <a class="btn btn-info btn-fill btn-block btn-kale-white center" href="<?php echo base_url();?>page/quienes-somos">Ver mas</a>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <img class="img-responsive" src="../assets/img/home-img.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
</html>
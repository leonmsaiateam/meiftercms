<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">
<?php $this->load->view('elements/navbar');?>
<div class="wrapper">

    <div class="parallax">
        <div class="parallax-image">
            <img src="<?php echo base_url(); ?>assets/img/blogbg.jpg" alt="..." />
        </div>
    </div>

     <div class="section blog">
       <div class="container">
           <div class="row">
              <?php foreach ($posts->result() as $pst): ?>
                <div class="col-md-4">
                    <div class="card card-plain">
                        <div class="image">
                            <img src="<?php echo base_url() . 'assets/uploads/files/post/' . $pst->imagen_assoc_id; ?>" alt="...">
                            <div class="filter">
                                <button type="button" class="btn btn-neutral btn-simple">
                                    <a href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>">
                                        <i class="fa fa-align-left"></i> Leer Noticia
                                    </a>
                                </button>
                            </div>
                        </div>
                        <div class="content">
                            <p class="category text-info">
                                <?php echo getCatPostNfo($pst->categorie_id)['title']; ?>
                            </p>
                            <a class="card-link" href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>">
                                <h4 class="title kaleTitle"><?php echo $pst->title; ?></h4>
                                <p><?php echo $pst->description; ?></p>
                            </a>
                             <div class="footer">
                                <div class="author">

                                    <a class="card-link" href="<?php echo base_url() . 'blog/post/' . $pst->slug; ?>">
                                       <img src="<?php echo getUserNfo($pst->id)['userPic'];?>" alt="..." class="avatar" />
                                       <span><?php echo getUserNfo($pst->id)['username'];?></span>
                                    </a>
                                </div>
                                 <!-- <div class="stats pull-right">
                                     <a class="card-link" href="#">
                                        <i class="fa fa-heart"></i> 92
                                     </a>
                                </div>
                                <div class="stats pull-right">
                                    <a class="card-link" href="#">
                                        <i class="fa fa-comment"></i> 41
                                    </a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
              <?php endforeach ?>

           </div>

           <div class="row">
             <div class="col-md-12">
              <ul id="navigation-pages" class="pagination pagination-no-border paginationKale">
                <li><a href="#">«</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">»</a></li>
              </ul>
             </div>
           </div>
           
       </div>
    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
<input type='hidden' id='current_page' />  
<input type='hidden' id='show_per_page' /> 
</body>
  <?php $this->load->view('assets/scripts');?>

  <script type="text/javascript">


    //Vagination
    var show_per_page = 2;
    var number_of_items  = '<?php echo count($produs) ?>';
    var number_of_pages = Math.ceil(number_of_items /show_per_page);

    //set the value of our hidden input fields  
    $('#current_page').val(0);  
    $('#show_per_page').val(show_per_page);

    var navigation_html =  '<li><a class="prev page-numbers" href="javascript:previous();">«</a></li>';
    var current_link = 0;  
    while(number_of_pages > current_link){  
        navigation_html += '<li><a class="page_link page-numbers" href="javascript:go_to_page(' + current_link +')" longdesc="' + current_link +'">'+ (current_link + 1) +'</a></li>';  
        current_link++;  
    }  
    navigation_html += '<li><a class="next page-numbers" href="javascript:next();">»</a></li>';  
    
    $('#navigation-pages').html(navigation_html);      
    
    //add active_page class to the first page link  
    $('#navigation-pages .page_link:first').parent().addClass('active');  
  
    //hide all the elements inside content div  
    $('#prods-list').children().css('display', 'none');  
    
    //and show the first n (show_per_page) elements  
    $('#prods-list').children().slice(0, show_per_page).css('display', 'block');      

    function previous(){  
    
        new_page = parseInt($('#current_page').val()) - 1;  
        console.log("LA NEW PAGE", new_page);
        //if there is an item before the current active link run the function  
        if($('.active').prev().children('.page_link').length==true){  
            go_to_page(new_page);  
        }  
      
    }  
      
    function next(){  
      new_page = parseInt($('#current_page').val()) + 1;  
        //if there is an item after the current active link run the function  
        if($('.active').next().children('.page_link').length==true){  
            go_to_page(new_page);  
        }  
      
    }  
    function go_to_page(page_num){  

        //get the number of items shown per page  
        var show_per_page = parseInt($('#show_per_page').val());  
      
        //get the element number where to start the slice from  
        var start_from = page_num * show_per_page;  
      
        //get the element number where to end the slice  
        var end_on = start_from + show_per_page;  
      
        //hide all children elements of content div, get specific items and show them  
        $('#prods-list').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');  
              
        $('.active').removeClass('active');

        /*get the page link that has longdesc attribute of the current page and add active_page class to it 
        and remove that class from previously active page link*/  
       // $('.page_link[longdesc=' + page_num +']').addClass('current currentPage').siblings('.currentPage').removeClass('currentPage current');  
        $('.page_link[longdesc=' + page_num +']').parent().addClass('active');
      
        //update the current page input field  
        $('#current_page').val(page_num);  
    } 


  </script>
</html>
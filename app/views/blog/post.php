<!doctype html>
<html lang="<?php echo $language;?>">
<head>
    <?php $this->load->view('assets/headnfo');?>
    <?php $this->load->view('assets/style');?>
</head>
<body class="home">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.6&appId=885482321490748";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php $this->load->view('elements/navbar');?>
<div class="wrapper">

    <?php foreach ($post->result() as $pt): ?> 

    <div class="section">
          
            <div class="container">
                 <div class="row">
                  
                  <div class="col-md-6">
                    <h1 class="title title-blog"><?php echo $pt->title; ?></h1>
                    <div class="bodyDesc">
                      <strong><?php echo $pt->description; ?></strong>
                    </div>
                    <div class="bodyNote">
                      <?php echo $pt->body; ?>
                    </div>
                    <hr>
                    <?php
                      $tagsArray = explode(';', $pt->tags);
                    ?>
                    <?php foreach ($tagsArray as $key => $value): ?>
                      <span class="label label-warning label-fill"><?php echo $value; ?></span>  
                    <?php endforeach ?>
                    <h3 class="title title-blog">Autor</h3>
                    <div class="media media-author">
                        <a class="pull-left" href="#">
                          <div class="avatar">
                              <img class="media-object" src="<?php echo getUserNfo($pt->post_id)['userPic'];?>" alt="<?php echo getUserNfo($pt->post_id)['username'];?>">
                          </div>
                        </a>
                        <div class="media-body">
                          <h6 class="media-heading"><?php echo getUserNfo($pt->post_id)['username'];?></h6>
                          <p class="text-muted"><?php echo getUserNfo($pt->post_id)['email'];?></p>
                        </div>
                        <p><?php echo getUserNfo($pt->post_id)['about'];?></p>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <?php $postIMG = base_url() . 'assets/uploads/files/post/' . $pt->imagen_assoc_id; ?>
                    <img src="<?php echo $postIMG?>" alt="..." />
                  </div>

              </div>
            </div>
          <?php endforeach ?>
          
          <?php
            $facebookAppID = 1009869059067304;
            $urlTarget = current_url();
            $numberPost = 5;
          ?>
        
          <div class="section">
            <div class="container container-comments">
              <div class="social-buttons">
                <?php makeFbBtn($urlTarget, 1); ?>
                <?php makeTwBtn($urlTarget, 1); ?>
                <?php makePntBtn($postIMG, $urlTarget, 1) ?>
              </div>
              <?php if (checkRelProdByPost($pt->post_id) == true): ?>
                <div class="row">
                  <div class="col-md-12">
                    <h3>Relacionados</h3>
                    <div class="row">
                      <?php getRelProdByPost($pt->post_id); ?>
                    </div>
                  </div>
                </div>
              <?php endif ?>
              <div class="row">
                <div class="col-md-12">
                  <h3 class="title">Comentarios</h3>
                  <div class="row">
                      <div class="media-area">
                        <div class="fb-comments" data-href="<?php echo $urlTarget;?>" data-numposts="<?php echo $numberPost;?>"></div>
                      </div>
                  </div>
                </div>
              </div>

            </div>
          </div>


    </div>

    <div class="space-50"></div>
    <?php $this->load->view('elements/footer');?>
</div>
</body>
  <?php $this->load->view('assets/scripts');?>
  <script type="text/javascript">
        var big_image;
        $().ready(function(){
            responsive = $(window).width();

            if (responsive >= 768){
                big_image = $('.parallax-image').find('img');

                $(window).on('scroll',function(){
                    parallax();
                });
            }
        });
       var parallax = function() {
            var current_scroll = $(this).scrollTop();

            oVal = ($(window).scrollTop() / 3);
            big_image.css('top',oVal);
        };
    </script>
</html>